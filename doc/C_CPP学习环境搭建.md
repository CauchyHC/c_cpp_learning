# C/C++学习环境搭建

**常见的C/C++学习环境搭建方式**

* 集成开发环境（IDE），代码的编辑、编译和调试都在同一个软件中完成
* windows下使用代码编辑器+编译器+插件
* Linux环境下使用vim+gcc+命令行+Makefile

**常见的C/C++集成开发环境**

* visual studio（windowds、macOS）
* CodeBlocks（windows、Linux、macOS）
* qt creator（windows、Linux、macOS）
* cfree（windows）
* dev-C++（windows）

**常见的代码编辑器**

* vscode
* source insight
* sublime text
* vim
* Clion
* Emacs
* ATOM

**常见的C/C++编译器**

* GCC/G++
* MSVC
* LLVM clang
* mingw64

**GCC来历**

GCC是GUN 开源组织开发出来的一款编译器。开始的时候GCC 的全拼为 GNU C Compiler，定位是用来编译 C 语言，但经过这些年不断的迭代，GCC 的功能得到了很大的扩展，它不仅可以用来编译 C 语言程序，还可以处理 C++、Go、Objective -C 等多种编译语言编写的程序。与此同时，由于之前的 GNU C Compiler 已经无法完美诠释 GCC 的含义，所以其英文全称被重新定义为 GNU Compiler Collection，即 GNU 编译器套件。

**GCC编译工具链组成**

gcc编译器是gcc编译工具链中的一部分，完成程序的编译（即将源代码编译成可执行文件）需要用到编译工具链，而不单是编译器。完整的gcc编译工具链(`toolchain`)由以下3个部分组成：

* **Gcc-Core：**GCC编译器，完成预处理和编译过程，将C代码转换为汇编
* **Binutils：**包括了链接器ld，汇编器as，目标文件格式查看器readelf等一系列小工具
* **glibc：**包含C语言标准库，C中常使用的`printf`、`malloc`等函数

**mingw64与gcc的关系**

mingw 的全称是：Minimalist GNU on Windows 。它实际上将Linux上的 GCC 移植到了 Windows 平台下，并且包含了 Win32API ，因此可以将源代码编译为可在 Windows 中运行的可执行程序。而且还可以使用一些 Windows 不具备的，Linux平台下的开发工具。一句话来概括：mingw就是 GCC 的 Windows 版本 。

mingw分为mingw64和mingw32两个版本，区别是mingw64可以生成64位和32位可执行程序，而mingw32只能生成32位可执行程序。

## CodeBlocks环境搭建

> 为什么选择CodeBlocks来学习C/C++?
>
> 原因：
>
> 1. 软件简单易用
> 2. 软件安装包小
> 3. 软件开源免费

**CodeBlocks下载**

[ CodeBlocks]:https://www.codeblocks.org/downloads/binaries/

![image-download-format](./download-format.png)

下载的时候可以选择包含mingw编译器的版本进行下载，这样以后如果想在windows上使用代码编辑器+mingw64的方式的话就不用再去下在mingw64编译器了，因为是外网的原因，在mingw64官网或者sourceforge下载mingw64速度非常慢。

**CodeBlocks安装**

![image-codeblocks-install-setup](./codeblocks-install-setup.png)

下载好之后，找到下载的位置点击安装包进行安装。安装过程中可以修改一下安装位置，需要注意的点如下

* 最好不装载系统盘（C盘）上；
* 安装路径中最好不要用中文名称；

除了以上两点需要注意，其他的可以不做修改，一路next知道安装完成就可以了。

**新建工程**

安装好之后，打开软件，点击菜单栏file->new->project新建工程

![image-newproject-1](./codeblocks-newproject-1.png)

执行完上图第3步后弹出如下配置对话框，只是学习C/C++的话选择Console application就行了，这个配置的意思是最终生成的可执行程序在控制台中执行（比如cmd、powershell这样的黑框框中）。

![image-newproject-2](./codeblocks-newproject-2.png)

点击上图的GO按钮后进行下一步，如下图所示

![image-newproject-3](./codeblocks-newproject-3.png)

上图中这一步不做修改，直接next

![image-newproject-4](./codeblocks-newproject-4.png)

上图中这一步用来选择要创建C工程还是C++工程，根据自己要学习的语言进行选择。这里以选择C语言为例，点击next进行下一步。

![image-newproject-5](./codeblocks-newproject-5.png)

上图这一步用来配置工程名称和工程保存位置，做好修改后点击next进行下一步。

![image-newproject-6](./codeblocks-newproject-6.png)

上图这一步用来完成以下的配置

* 选择一款编译器，如果不知道如何选择，使用默认选择即可
* 是否创建调试配置，如果选中的话，在编译的时候会加上`-g`选项生成调试信息，最终生成的可执行文件放在当前工程目录的bin\Debug文件夹下
* 是否创建发行配置，这个选项一般用于发行最后调试好的版本，编译时没有`-g`选项，所以生成的可执行文件比创建调试配置下生成的可执行文件大小要小，最终生成的可执行文件放在当前目录的bin\Release文件夹下

点击finish后完成工程的新建，CodeBlocks自动为新建的工程添加了`mian.c`文件，如下图所示

![image-newproject-7](./codeblocks-newproject-7.png)



**修改主题配置文件**

刚安装好CodeBlocks的时候，代码编辑区的配置是白色的，如下图所示：

![image-editor-default-config](./editor-default-config.png)

白色的背景配置比较伤眼睛，如果不喜欢这样的配置，我们可以去网上搜索一些CodeBlocks的主题配色来替换默认的主题。

![image-colour-theme](./colour-theme.png)

下载好主题配置文件后，如上图所示，在CodeBlocks安装目录下，找到`cb_share_config.exe`这个文件，点击运行。需要注意的是，在运行这个文件之前需要先把CodeBlocks关了。

![image-colour-theme-2](./colour-theme-2.png)

在上图中完成如下配置：

* 在步骤1中选择自己下载好的主题配置文件
* 在步骤2中选择原来的配置文件，这个文件在`C:\Users\haung\AppData\Roaming\CodeBlocks`文件夹下，路径中的`huang`是我的用户名，这里需要修改成自己的，其他不变
* 在步骤3中勾选全部的选项，注意，右边有一个可以下拉的滚动条，往下拉还有一些选项，不要只勾选了一部分选项
* 在步骤4中点击Transfer将左边的配置导入到右边的配置文件中
* 在步骤5中点击保存修改
* 在步骤6中点击关闭

完成主题配色文件的修改后，就可以选择一款自己喜好的主题来使用。在标题栏中点击Setting->editor，

如下图所示

![image-colour-theme-3](./colour-theme-3.png)

点击Editor后进入如下的配置界面

![image-colour-theme-4](./colour-theme-4.png)

在上图中完成主题配色的选择，比如这里选择sublime主题。修改主题后，可能光标闪烁位置会看不见，同样点击菜单栏Setting->editor后进行如下图所示修改

![image-colour-theme-5](./colour-theme-5.png)



**代码的编译与运行**

编辑好源文件后，在工具栏找到如下图所示位置

![image-build-run](./build-run.png)

上图中的几个图标解释如下

* 图标1（build）用于编译源文件
* 图标2（run）用来运行编译后得到的可执行文件
* 图标3（build and run）执行编译并运行

点击build按钮后，可以在软件下方的build log窗口内看到编译信息，如下图所示

![image-build-log](./build-log.png)

点击run按钮后，输出打印语句如下图所示

![image-run](./run.png)

上图可以看到，程序正确打印出了在`main.c`文件的`main`函数中的`printf("Hello world!\n")`语句，按任意键两次后可以退出控制台。

**代码调试**

想要调试代码，需要添加断点（让程序运行到这个点停下来），然后重新编译后才能调试，断点可以添加多个。添加断点的方式如下图所示，在代码行的后面点击鼠标左键会出现一个红点，表示一个断点，再次点击红点消失，表示取消断点。

![image-breakpoint](./breakpoint.png)

在上图中的10行和12行位置添加了两个断点，添加好后点击build按钮重新编译就可以进行调试了。调试相关的按钮如下图所示

![image-debug](./debug.png)

上图中的各个按钮用途如下

* 按钮1（Debug/continue）用于启动调试
* 按钮2（run to cursor）跳光标处，即将代码运行到当前光标所在行
* 按钮3（next line）为单步执行按钮，按一次执行到下一行位置，不进入所调用的函数内部
* 按钮4（step into），按下后进入到调用的函数内部
* 按钮5（step out）跳出函数，如果前面按下了step into按钮，可以按step out按钮跳出函数
* 按钮6（next instruction）执行下一条指令
* 按钮7（step into instruction）进入指令内部
* 按钮8（break Debugger），停止当前正在运行的程序
* 按钮9（stop Debugger）停止调试

如果想查看程序运行过程中变量的当前值，可以打开watch窗口，打开方式如下如所示

![image-watch-window](./watch-window.png)

打开watch窗口后，会自动将·main`函数中的变量添加到watch窗口中。如果想要查看的变量位于其他函数中，需要在这个函数内打断点，然后鼠标选中这个变量后鼠标右键，选择watch ‘variable-name’将变量添加到watch窗口，如下如所示

![image-other-function-watch-var](./other-function-watch-var.png)

**添加源文件到当前工程目录下**

![image-add-sourcefile-to-projectdir-1](./add-sourcefile-to-projectdir-1.png)

![image-add-sourcefile-to-projectdir-2](./add-sourcefile-to-projectdir-2.png)

![image-add-sourcefile-to-projectdir-3](./add-sourcefile-to-projectdir-3.png)

![image-add-sourcefile-to-projectdir-4](./add-sourcefile-to-projectdir-4.png)

![image-add-sourcefile-to-projectdir-5](./add-sourcefile-to-projectdir-5.png)

![image-add-sourcefile-to-projectdir-6](./add-sourcefile-to-projectdir-6.png)

![image-add-sourcefile-to-projectdir-7](./add-sourcefile-to-projectdir-7.png)

**添加源文件到当前工程下的子目录中**

添加源文件到子目录中的方式和添加到当前工程目录下差不多，区别在选择`Filename with full path`步骤中，也就是上面步骤中倒数第二图开始有区别，具体如下图所示

![image-add-sourcefile-to-subdir-1](./add-sourcefile-to-subdir-1.png)

![image-add-sourcefile-to-subdir-2](./add-sourcefile-to-subdir-2.png)

![image-add-sourcefile-to-subdir-3](./add-sourcefile-to-subdir-3.png)

