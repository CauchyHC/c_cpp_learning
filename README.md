# c_cpp_learning

## 介绍

本仓库用于学习C/C++的软件环境搭建，介绍windows下集成开发环境CodeBlocks，windows下vscode+mingw64，Linux下vim+gcc+Makefile的环境搭建方法

## Windows集成开发环境搭建

介绍windows下集成开发环境CodeBlocks的搭建，涉及内容如下

* 软件的下载与安装
* 更改软件主题配色
* 新建工程
* 程序的编译与运行
* 程序的调试
* 添加源文件

## Windows下vscode环境搭建

介绍内容如下

* 软件的下载与安装
* 代码编辑相关设置
* C/C++常用插件介绍
* mingw64编译器的使用
* 程序的编译与运行
* 程序的调试
* 在vscode中使用git bash

## Linux下C/c++环境搭建

介绍内容如下

* vim基本操作
* gcc命令的使用
* 简单Makefile的编写

